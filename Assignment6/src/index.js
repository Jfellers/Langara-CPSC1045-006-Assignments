//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function boxprocess(x, y, svg, bnn, bsn, bgn) {
    let bstring = '<rect x="' + x + '" y="' + y + '" width="' + bgn * bsn + '"' + ' height="' + bgn * bsn + '"' + ' Fill="black"/>';
    svg += bstring
    return svg;
}


function boxinput() {
    let bn = document.querySelector("#bnumber");
    let bs = document.querySelector("#bsize");
    let bg = document.querySelector("#bgrowth");
    let svg = ('<svg height="200" width="400">');
    let bnn = Number(bn.value);
    let bsn = Number(bs.value);
    let bgn = Number(bg.value);
    let x = 20
    let y = 0
    let boutput = document.querySelector("#boutput");
    


    for (let z = 0; z < bnn; z++ ) {
        x = x + 40 + bnn + bgn;
        bgn = bgn + 10;

        svg = boxprocess(x, y, svg, bnn, bsn, bgn);
        //todo chanage width and height 10 20 30 40...
    }

    svg += "</svg>";
    boutput.innerHTML = svg;
    console.log(boutput);
}
window.boxinput = boxinput;