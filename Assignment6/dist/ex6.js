let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
   let XV;
   let count =0;
    for (XV = 0; XV < 8; XV++){
    count = count + containers[XV].innerHTML.length;
    containers[XV].innerHTML = containers[XV].innerHTML.toUpperCase();
    }
   
    

    //Output Part 1: You do not need to change this. 
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;
  let YV;
 for (YV=0; YV <7; YV++){
    x = x + 10;
    y = 200 - heights[YV] * 20;
  
    pointString = pointString + x + "," + y + " ";
 }

    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;
    for (XZ = 0; XZ < 4; XZ++ ){
    x = data[XZ];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;
}


    svgString += "</svg>";
    let output3 = document.querySelector("#output3");
    output3.innerHTML += svgString;

}




