//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function click(){
 let x = event.offsetX;
 let y = event.offsetY;
 let c = document.querySelector("#color").value;
//  console.log(c);
 let svg = document.querySelector("#svg");
//  console.log(svg);
 let circlestring = '<circle cx="'+x+'" cy="'+y+'" fill="'+c+'" r="30" />';
//  console.log(circlestring);
 svg.innerHTML += circlestring;
//  console.log(svg.innerHTML);
}

window.click = click