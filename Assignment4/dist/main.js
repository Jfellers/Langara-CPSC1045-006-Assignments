/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function update(){
let x = document.querySelector("#x");
let y = document.querySelector("#y");
let X = Number(x.value);
let Y = Number(y.value);
let slider = document.querySelector("#Angle");
let translatestring =" translate("+X+" "+Y+")";
let rotatestring =" rotate("+slider.value+ " 0  0)";
let transformstring = translatestring+" "+rotatestring;
let group = document.querySelector("#svg");
//console.log(transformstring);
group.setAttribute("transform", transformstring);







}
    

function update2(){
let x1 = document.querySelector("#x1");
let y1 = document.querySelector("#y1");
let x2 = Number(x1.value);
let y2 = Number(y1.value);
let group1 = document.querySelector("#polly");
let translate1 =" translate("+x2+" "+y2+")"
let ri = document.querySelector("#in");
let ro = document.querySelector("#out");
let ro1 = Number(ro.value);
let ri1 = Number(ri.value);
let ply = document.querySelector("#polygon");
let p1 = ro1*Math.cos(0)+" "+ro1 * Math.sin(0);
let p2 = ri1*Math.cos(0.76)+" "+ri1 * Math.sin(0.76);
let p3 = ro1*Math.cos(1.57)+" "+ro1*Math.sin(1.57);
let p4 = ri1*Math.cos(2.36)+" "+ri1*Math.sin(2.36);
let p5 = ro1*Math.cos(3.14)+" "+ro1*Math.sin(3.14);
let p6 = ri1*Math.cos(3.92)+" "+ri1*Math.sin(3.92);
let p7 = ro1*Math.cos(4.71)+" "+ro1*Math.sin(4.71);
let p8 = ri1*Math.cos(5.50)+" "+ri1*Math.sin(5.50);
let path = p1+","+p2+","+p3+","+p4+","+p5+","+p6+","+p7+","+p8;
ply.setAttribute("points", path);
group1.setAttribute("transform", translate1);
    console.log(ro, ri, ro1, ri1)





}
window.update = update
window.update2 = update2

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBLHdCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsIi8vVGhpcyBpcyB0aGUgZW50cnkgcG9pbnQgSmF2YVNjcmlwdCBmaWxlLlxyXG4vL1dlYnBhY2sgd2lsbCBsb29rIGF0IHRoaXMgZmlsZSBmaXJzdCwgYW5kIHRoZW4gY2hlY2tcclxuLy93aGF0IGZpbGVzIGFyZSBsaW5rZWQgdG8gaXQuXHJcbmNvbnNvbGUubG9nKFwiSGVsbG8gd29ybGRcIik7XHJcblxyXG5mdW5jdGlvbiB1cGRhdGUoKXtcclxubGV0IHggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3hcIik7XHJcbmxldCB5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN5XCIpO1xyXG5sZXQgWCA9IE51bWJlcih4LnZhbHVlKTtcclxubGV0IFkgPSBOdW1iZXIoeS52YWx1ZSk7XHJcbmxldCBzbGlkZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI0FuZ2xlXCIpO1xyXG5sZXQgdHJhbnNsYXRlc3RyaW5nID1cIiB0cmFuc2xhdGUoXCIrWCtcIiBcIitZK1wiKVwiO1xyXG5sZXQgcm90YXRlc3RyaW5nID1cIiByb3RhdGUoXCIrc2xpZGVyLnZhbHVlKyBcIiAwICAwKVwiO1xyXG5sZXQgdHJhbnNmb3Jtc3RyaW5nID0gdHJhbnNsYXRlc3RyaW5nK1wiIFwiK3JvdGF0ZXN0cmluZztcclxubGV0IGdyb3VwID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzdmdcIik7XHJcbi8vY29uc29sZS5sb2codHJhbnNmb3Jtc3RyaW5nKTtcclxuZ3JvdXAuc2V0QXR0cmlidXRlKFwidHJhbnNmb3JtXCIsIHRyYW5zZm9ybXN0cmluZyk7XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG59XHJcbiAgICBcclxuXHJcbmZ1bmN0aW9uIHVwZGF0ZTIoKXtcclxubGV0IHgxID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN4MVwiKTtcclxubGV0IHkxID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN5MVwiKTtcclxubGV0IHgyID0gTnVtYmVyKHgxLnZhbHVlKTtcclxubGV0IHkyID0gTnVtYmVyKHkxLnZhbHVlKTtcclxubGV0IGdyb3VwMSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcG9sbHlcIik7XHJcbmxldCB0cmFuc2xhdGUxID1cIiB0cmFuc2xhdGUoXCIreDIrXCIgXCIreTIrXCIpXCJcclxubGV0IHJpID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNpblwiKTtcclxubGV0IHJvID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNvdXRcIik7XHJcbmxldCBybzEgPSBOdW1iZXIocm8udmFsdWUpO1xyXG5sZXQgcmkxID0gTnVtYmVyKHJpLnZhbHVlKTtcclxubGV0IHBseSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcG9seWdvblwiKTtcclxubGV0IHAxID0gcm8xKk1hdGguY29zKDApK1wiIFwiK3JvMSAqIE1hdGguc2luKDApO1xyXG5sZXQgcDIgPSByaTEqTWF0aC5jb3MoMC43NikrXCIgXCIrcmkxICogTWF0aC5zaW4oMC43Nik7XHJcbmxldCBwMyA9IHJvMSpNYXRoLmNvcygxLjU3KStcIiBcIitybzEqTWF0aC5zaW4oMS41Nyk7XHJcbmxldCBwNCA9IHJpMSpNYXRoLmNvcygyLjM2KStcIiBcIityaTEqTWF0aC5zaW4oMi4zNik7XHJcbmxldCBwNSA9IHJvMSpNYXRoLmNvcygzLjE0KStcIiBcIitybzEqTWF0aC5zaW4oMy4xNCk7XHJcbmxldCBwNiA9IHJpMSpNYXRoLmNvcygzLjkyKStcIiBcIityaTEqTWF0aC5zaW4oMy45Mik7XHJcbmxldCBwNyA9IHJvMSpNYXRoLmNvcyg0LjcxKStcIiBcIitybzEqTWF0aC5zaW4oNC43MSk7XHJcbmxldCBwOCA9IHJpMSpNYXRoLmNvcyg1LjUwKStcIiBcIityaTEqTWF0aC5zaW4oNS41MCk7XHJcbmxldCBwYXRoID0gcDErXCIsXCIrcDIrXCIsXCIrcDMrXCIsXCIrcDQrXCIsXCIrcDUrXCIsXCIrcDYrXCIsXCIrcDcrXCIsXCIrcDg7XHJcbnBseS5zZXRBdHRyaWJ1dGUoXCJwb2ludHNcIiwgcGF0aCk7XHJcbmdyb3VwMS5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIiwgdHJhbnNsYXRlMSk7XHJcbiAgICBjb25zb2xlLmxvZyhybywgcmksIHJvMSwgcmkxKVxyXG5cclxuXHJcblxyXG5cclxuXHJcbn1cclxud2luZG93LnVwZGF0ZSA9IHVwZGF0ZVxyXG53aW5kb3cudXBkYXRlMiA9IHVwZGF0ZTIiXSwic291cmNlUm9vdCI6IiJ9