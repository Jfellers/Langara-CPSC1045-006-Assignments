//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log()
function starfunction(np, ir, or){
    let star = [ ];
    let angle=0;
    let subangle=(360/2*np*Math.PI / 180);
    let oradius={x:300*(Math.cos(angle)), y:300*Math.sin(angle)}
    let iradius={x:300*Math.cos(subangle), y:300*Math.sin(subangle)}
    let a = 0 

    for( a = 0; a>2*np; a++){
        if (a%2==0){
            star.push(oradius.x+ " ");
            star.push(oradius.y+ " "); 
            angle+=((360/np)*Math.PI / 180);
            oradius={x:300*(Math.cos(angle)), y:300*Math.sin(angle)}
        }

        else {
            star.push(iradius.x, " ");
            star.push(oradius.y, " ");
            subangle+=((360/2*np)*Math.PI / 180)*2;
            iradius={x:300*Math.cos(subangle), y:300*Math.sin(subangle)}
        }




}
return star;
}


function arrayfunction(star){ 
let pointstring = "";
for(let z=0; z<(star.length); z++) {
 pointstring += star[z];
}
return pointstring;
}


function inputfunction(){                
let nnp = document.querySelector("#input");
let np = Number(nnp.value);
let or = 200;
let ir = 100;
let poly = document.querySelector("#poly");
let star = starfunction(np,ir, or) ;
let pointstring = arrayfunction(star);
poly.setAttribute("points", pointstring);
}

window.inputfunction=inputfunction;